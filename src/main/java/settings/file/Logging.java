package settings.file;

import java.time.LocalDate;
import java.util.Enumeration;

import org.apache.log4j.FileAppender;
import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class Logging {
	private static Logger logger;

	public static void log(String message, String ext, String fileName,String level) {
		if (logger == null)
			config();
		level=level.toUpperCase();
		setLogFileName(level,ext, fileName);
		logger.log(Level.toLevel(level),message);
	}

	private static void config() {
		logger = LogManager.getLogger(Logging.class);
	}

	private static void setLogFileName(String level,String ext, String fileName) {
		if (fileName != null && ext != null) {
			Enumeration<?> x = Logger.getRootLogger().getAllAppenders();
			FileAppender fa = (FileAppender) x.nextElement();
			String fPath=fa.getFile();
			String path=fPath.substring(0,fPath.lastIndexOf("\\")+1);
			fa.setFile(path+"/"+LocalDate.now()+"/"+level+"/"+fileName + "." + ext);
			fa.activateOptions();

		}
	}

}
